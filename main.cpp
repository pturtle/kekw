#include <iostream>
#include <cstdlib>
using namespace std;

struct Node {
  int value;
  int zeroes;
  struct Node* next;
};

Node* create() {
  Node* node = (Node*)malloc(sizeof(Node));
  node->value = 0;
  node->zeroes = 0;
  node->next = 0;
  return node;
}

void print_matrix(int** arr, int lines) {
  int size;
  puts("*** Result array ***");
  for (int i = 0; i < lines; i++) {
    size = arr[i][0];
    for (int j = 1; j < size + 1; j++) {
      printf("%d ", arr[i][j]);
    }
    putchar('\n');
  }
}

void print_zeroes(int amount) {
  for (int i = 0; i < amount; i++) {
    printf("0 ");
  }
}

void print_nodes(Node** arr, int lines) {
  Node* cur;
  puts("*** Source array ***");
  for (int i = 0; i < lines; i++) {
    cur = arr[i];
    for (;;) {
      if (cur->zeroes) {
        print_zeroes(cur->zeroes);
      }
      if (cur->value) {
        printf("%d ", cur->value);
      } else {
        putchar('\n');
        break;
      }
      cur = cur->next;
    }
  }
}

int main() {
  int m = 0, n = 0, current = 0;
  int real_m = 0, real_n = 0;
  int is; // 0 - don't add; 1 - do add; 2 - don't add anymore;
  int** res_arr = 0;
  Node** src_arr = 0;
  Node* last = 0;
  cout << "Input m>>";
  cin >> m;
  cout << "Input n>>";
  cin >> n;

  for (int i = 0; i < m; i++) {
    is = 0;
    real_n = 0;

    src_arr = (Node**)realloc(src_arr, sizeof(Node*) * (i + 1));
    src_arr[i] = create();
    last = src_arr[i];
    for (int j = 0; j < n; j++) {
      printf("Input (%d, %d) >> ", i, j);
      cin >> current;
      if (!current) {
        last->zeroes++;
      } else {
        last->next = create();
        last->value = current;
        last = last->next;
      }
      if (!is && (current > 0)) {
        is = 1;
        res_arr = (int**)realloc(res_arr, sizeof(int*) * (real_m + 1));
        res_arr[real_m] = (int*)realloc(res_arr[real_m], sizeof(int) * (real_n + 1));
        res_arr[real_m][0] = 0;
        real_n++;
        continue;
      }
      if (is == 1) {
        if (current >= 0) {
          res_arr[real_m] = (int*)realloc(res_arr[real_m], sizeof(int) * (real_n + 1));
          res_arr[real_m][real_n] = current;
          res_arr[real_m][0]++;
        } else {
          is = 2;
        }
        real_n++;
      }
    }
    if (is) {
      real_m++;
    }
  }
  putchar('\n');
  print_nodes(src_arr, m);
  putchar('\n');
  print_matrix(res_arr, real_m);
}
